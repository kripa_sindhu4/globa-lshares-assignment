package com.globalshare.test.globalshare;

/**
 * Created by kripasindhu on 3/27/19.
 */

public class SharePriceModal {
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    private String symbol;

    private String name;

    private double value;
}
