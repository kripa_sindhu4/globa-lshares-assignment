package com.globalshare.test.globalshare;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kripasindhu on 3/27/19.
 */

public class CertificateDataApdapter extends RecyclerView.Adapter<CertificateDataApdapter.MovieViewHolder> {

    private List<CertiFicateModal> certiFicateModalLists;
    private int rowLayout;
    private Context context;
    private  double currentSharePrice;
    HashMap<Integer,Integer>  mapNumberOfShares = new HashMap<Integer,Integer>();
    HashMap<Integer,Double>  mapValueOfShares = new HashMap<Integer,Double>();
    private boolean emptyEdittext;
    private static int finalNumberofshares =0;
    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        LinearLayout parentLinear;
        TextView certificateCount,numberOfSharesCount,dateIssued;
        EditText sharesToselleditText;
        TextView valueofShares;


        public MovieViewHolder(View v) {
            super(v);
            parentLinear = (LinearLayout) v.findViewById(R.id.parent_linear) ;
            certificateCount = (TextView) v.findViewById(R.id.certificate_count);
            numberOfSharesCount = (TextView) v.findViewById(R.id.number_of_shares_count);
            sharesToselleditText = (EditText) v.findViewById(R.id.sell_share_count);
            valueofShares = (TextView) v.findViewById(R.id.value_of_share);
            dateIssued = (TextView)v.findViewById(R.id.date_issued);

        }
    }

    public CertificateDataApdapter(List<CertiFicateModal> certiFicateModalLists, int rowLayout, Context context,double currentSharePrice) {
        this.certiFicateModalLists = certiFicateModalLists;
        this.rowLayout = rowLayout;
        this.context = context;
        this.currentSharePrice = currentSharePrice;

    }

    @Override
    public CertificateDataApdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MovieViewHolder holder, final int position) {
       try{
           String x = certiFicateModalLists.get(position).getIssuedDate(); // date conversion
           x = x.replaceAll("[^0-9]", "");
           DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
           long milliSeconds= Long.parseLong(x);
           Calendar calendar = Calendar.getInstance();
           calendar.setTimeInMillis(milliSeconds);
           holder.dateIssued.setText(formatter.format(calendar.getTime()));
       }catch (Exception e){
           e.printStackTrace();
       }
        holder.certificateCount.setText(String.valueOf(certiFicateModalLists.get(position).getCertificateId()));
        holder.numberOfSharesCount.setText(String.valueOf(certiFicateModalLists.get(position).getNumberOfShares()));



        holder. sharesToselleditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    if(!TextUtils.isEmpty(holder.sharesToselleditText.getText().toString().trim())){
                        double valueOfshares =  Double.valueOf(holder.sharesToselleditText.getText().toString())* currentSharePrice;
                        holder.valueofShares.setText(String.valueOf(String.format("%.2f", valueOfshares)));
                        mapNumberOfShares.put(position,Integer.parseInt(s.toString()));
                        mapValueOfShares.put(position,Double.parseDouble(holder.valueofShares.getText().toString()));
                        updateTotalValue(Integer.parseInt(s.toString()),certiFicateModalLists.get(position).getNumberOfShares());
                    }else {
                        emptyEdittext = true;
                        holder.valueofShares.setText("");
                        mapNumberOfShares.put(position,0);
                        mapValueOfShares.put(position,0.0);
                        updateTotalValue(0,0);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return certiFicateModalLists.size();
    }

    public static interface SendTotalShares{
        void sendTotalSharesCount(String totalShareCount,double valueOfshares,int numbersOfSharesToSell,int availableNumberOfShares);
    }

    private void updateTotalValue(int numbersOfSharesToSell,int availableNumberOfShares) {
        int sumNumberOfshares = 0;
        double sumValueofshares = 0.0;
        for (int total : mapNumberOfShares.values()) { // to get sum of number of shares to sell
            sumNumberOfshares += total;
        }
        for (double total : mapValueOfShares.values()) {  // to get sum of sharesvalue
            sumValueofshares += total;
        }
        SendTotalShares sendTotalShares =   (SendTotalShares) context;
        sendTotalShares.sendTotalSharesCount(String.valueOf(sumNumberOfshares),sumValueofshares,numbersOfSharesToSell,availableNumberOfShares);
    }



}
