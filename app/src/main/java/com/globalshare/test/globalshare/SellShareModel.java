package com.globalshare.test.globalshare;

/**
 * Created by kripasindhu on 3/27/19.
 */

public class SellShareModel {
    public String getValueOFshares() {
        return valueOFshares;
    }

    public void setValueOFshares(String valueOFshares) {
        this.valueOFshares = valueOFshares;
    }

    public String getNumbersOFSharesToSell() {
        return numbersOFSharesToSell;
    }

    public void setNumbersOFSharesToSell(String numbersOFSharesToSell) {
        this.numbersOFSharesToSell = numbersOFSharesToSell;
    }

    private String valueOFshares;
    private String numbersOFSharesToSell;

    public SellShareModel(String numbersOFSharesToSell, String valueOFshares) {
        this.numbersOFSharesToSell = numbersOFSharesToSell;
        this.valueOFshares = valueOFshares;
    }
}
