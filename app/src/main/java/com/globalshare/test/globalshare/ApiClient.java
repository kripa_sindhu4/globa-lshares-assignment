package com.globalshare.test.globalshare;

/**
 * Created by kripasindhu on 3/27/19.
 */

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String BASE_URL = "http://developerexam.equityplansdemo.com/test/";
    public static  final String SELL_SHARE_URL = "https://www.webhook.site/";
    private static Retrofit retrofit = null;
    private static Retrofit retrofitSellShare = null;

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public static Retrofit getClientForSellShare() {
        if (retrofitSellShare==null) {
            retrofitSellShare = new Retrofit.Builder()
                    .baseUrl(SELL_SHARE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitSellShare;
    }
}
