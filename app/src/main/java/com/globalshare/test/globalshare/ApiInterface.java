package com.globalshare.test.globalshare;

/**
 * Created by kripasindhu on 3/27/19.
 */

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @GET("sampledata")
    Call<List<CertiFicateModal>> getCertificateData();

    @GET("fmv")
    Call<SharePriceModal> getSharePrice();

    @POST("10e07a57-abce-4e08-91f2-c54d167d81b8")
    Call<SellShareModel> sendShareData(@Body SellShareModel user);

}
