package com.globalshare.test.globalshare;

/**
 * Created by kripasindhu on 3/27/19.
 */

public class CertiFicateModal {

    private String issuedDate;

    private int certificateId;

    private int numberOfShares;

    public String getIssuedDate ()
    {
        return issuedDate;
    }

    public void setIssuedDate (String issuedDate)
    {
        this.issuedDate = issuedDate;
    }

    public int getCertificateId ()
    {
        return certificateId;
    }

    public void setCertificateId (int certificateId)
    {
        this.certificateId = certificateId;
    }

    public int getNumberOfShares ()
    {
        return numberOfShares;
    }

    public void setNumberOfShares (int numberOfShares)
    {
        this.numberOfShares = numberOfShares;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [issuedDate = "+issuedDate+", certificateId = "+certificateId+", numberOfShares = "+numberOfShares+"]";
    }

}
