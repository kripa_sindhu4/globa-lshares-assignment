package com.globalshare.test.globalshare;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements CertificateDataApdapter.SendTotalShares {
    private RecyclerView recyclerView;
    private TextView sharePriceCount;
    private double currentSharePrice;
    private TextView toalSharesTextview,totalSharestoSell,totalValueofShares;
    int totaShares = 0;
    Button sellShareButton,canceButton;
    int numbersOFSharesToSell=0, availableNumberofShares =0;
    double valueOFshares = 0.0;
    private ProgressDialog dialog;
    LinearLayout bottomLinear;
    String totalshareCount = "";
    private boolean fiftyPercentageError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_certificate);
        sharePriceCount = (TextView) findViewById(R.id.share_price_count) ;
        toalSharesTextview = (TextView) findViewById(R.id.total_shares) ;
        totalSharestoSell = (TextView) findViewById(R.id.total_shares_to_sell) ;
        totalValueofShares = (TextView) findViewById(R.id.total_value_of_share) ;
        sellShareButton = (Button) findViewById(R.id.sale_share_button);
        canceButton = (Button) findViewById(R.id.cancel_button);
        dialog = new ProgressDialog(MainActivity.this);
        bottomLinear = (LinearLayout) findViewById(R.id.bottom_linear);
        sellShareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int fiftyPercentage = (int)(availableNumberofShares*(50.0f/100.0f));

                if(numbersOFSharesToSell <= 0){
                    Toast.makeText(MainActivity.this,"Please enter the number of shares to sell",Toast.LENGTH_LONG).show();
                    return;
                }
                if(numbersOFSharesToSell > availableNumberofShares ){
                    Toast.makeText(MainActivity.this,"You canot sell more than the available share",Toast.LENGTH_LONG).show();
                    return;
                }
                if(numbersOFSharesToSell > fiftyPercentage ){
                    fiftyPercentageError = true;
                    showAlertDialogue();
                    return;
                }
                if(fiftyPercentageError){

                }else {
                    hitSellSharesApi();
                }

            }
        });
        canceButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                numbersOFSharesToSell= 0;
                availableNumberofShares = 0;
                totalSharestoSell.setText("");
                totalValueofShares.setText("");
            }
        });


    }
    private void getSharePrice(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<SharePriceModal> call = apiService.getSharePrice();

        call.enqueue(new Callback<SharePriceModal>() {
            @Override
            public void onResponse(Call<SharePriceModal> call, Response<SharePriceModal> response) {
                sharePriceCount.setText(String.valueOf(response.body().getValue()));
                currentSharePrice = response.body().getValue();
               if(getSupportActionBar() != null){
                   getSupportActionBar().setTitle(response.body().getName());
               }

            }

            @Override
            public void onFailure(Call<SharePriceModal> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Some thing went wrong ! please try again later..",Toast.LENGTH_LONG).show();

            }
        });
    }

    private void callApiEvery30Sec(){
        Timer timer = new Timer ();
        TimerTask hourlyTask = new TimerTask () {
            @Override
            public void run () {
               getSharePrice();
            }
        };
        timer.schedule (hourlyTask, 0l, 30000);
    }

    private void getCertificateData(){
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);

    Call<List<CertiFicateModal>> call = apiService.getCertificateData();
        dialog.setMessage("Doing something, please wait.");
        dialog.show();
    call.enqueue(new Callback<List<CertiFicateModal>>() {
        @Override
        public void onResponse(Call<List<CertiFicateModal>> call, Response<List<CertiFicateModal>> response) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            try{
                for(int i = 0 ; i< response.body().size();i++){
                    totaShares = totaShares + response.body().get(i).getNumberOfShares();
                }
                toalSharesTextview.setText(String.valueOf(totaShares));
                CertificateDataApdapter  mAdapter = new CertificateDataApdapter(response.body(),R.layout.certificate_item,MainActivity.this,currentSharePrice);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setAdapter(mAdapter);
            }catch (Exception e){
                e.printStackTrace();
            }


        }

        @Override
        public void onFailure(Call<List<CertiFicateModal>> call, Throwable t) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Toast.makeText(MainActivity.this,"Some thing went wrong ! please try again later..",Toast.LENGTH_LONG).show();

        }
    });

}

    private void hitSellSharesApi(){
        dialog.setMessage("Doing something, please wait.");
        dialog.show();
        ApiInterface apiService =
                ApiClient.getClientForSellShare().create(ApiInterface.class);

        SellShareModel sellShareModel = new SellShareModel(totalshareCount,String.valueOf(valueOFshares));

        Call<SellShareModel> userCall = apiService.sendShareData(sellShareModel);
     userCall.enqueue(new Callback<SellShareModel>() {
    @Override
    public void onResponse(Call<SellShareModel> call, Response<SellShareModel> response) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        Toast.makeText(MainActivity.this,"Success: Request Recorded!",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(Call<SellShareModel> call, Throwable t) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        Toast.makeText(MainActivity.this,"Success: Request Recorded!",Toast.LENGTH_LONG).show();

    }
});
    }
    @Override
    public void sendTotalSharesCount(String totalShareCount,double valueOfshares,int numbersOfSharesToSell,int availableNumberOfShares) {
        totalSharestoSell.setText(totalShareCount);
        try{
            totalValueofShares.setText(String.valueOf(String.format("%.2f", valueOfshares)));
        }catch (Exception e){
           e.printStackTrace();
        }
        valueOFshares = valueOfshares;
        numbersOFSharesToSell = numbersOfSharesToSell;
        availableNumberofShares = availableNumberOfShares;
        totalshareCount = totalShareCount;
    }
    private void showAlertDialogue(){
        new AlertDialog.Builder(MainActivity.this)
                .setMessage("You are selling 50 percent of the available share? Do you want to continue")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        hitSellSharesApi();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard(MainActivity.this);
        getSharePrice();
        getCertificateData();
        callApiEvery30Sec();

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService( Context.INPUT_METHOD_SERVICE );
        View f = activity.getCurrentFocus();
        if( null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom( f.getClass() ) )
            imm.hideSoftInputFromWindow( f.getWindowToken(), 0 );
        else
            activity.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );

    }
}
